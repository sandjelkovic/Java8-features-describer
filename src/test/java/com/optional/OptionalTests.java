package com.optional;

import com.saanx.Address;
import com.saanx.AddressRepository;
import com.saanx.City;
import com.saanx.User;
import com.saanx.UserHasNoAddressException;
import com.saanx.service.ConfigurationService;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

public class OptionalTests {
	private User userWithAllFields;
	private User userWithNullAddress;
	private AddressRepository addressRepository = new AddressRepository();
	private ConfigurationService configurationService = new ConfigurationService();

	@Test
	public void testMapToTheEndNoNulls() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithAllFields);
		assertTrue(optUser.isPresent());

		Optional<Address> optAddress = optUser.map(User::getAddress);
		assertTrue(optAddress.isPresent());
		assertTrue(optAddress.get() instanceof Address);

		Optional<City> optCity = optAddress.map(Address::getCity);
		assertTrue(optCity.isPresent());
		assertTrue(optCity.get() instanceof City);

		Optional<String> optCityName = optUser
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName);
		assertTrue(optCityName.isPresent());
		assertEquals("Belgrade", optCityName.get());
	}

	@Test
	public void testMapToTheEndWithNulls() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithNullAddress);
		Optional<String> optCityName = optUser
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName);

		assertFalse(optCityName.isPresent());
	}

	@Test
	public void testStartingValueIsNull() throws Exception {
		User nullUser = null;
		Optional<String> optStreet = Optional.ofNullable(nullUser)
				.map(User::getAddress)
				.map(Address::getStreet);

		assertFalse(optStreet.isPresent());
	}

	@Test
	public void testMapToTheEndWithNullsDetailed() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithNullAddress);
//		Optional<String> optCityName = optUser
//				.map(User::getAddress)
//				.map(Address::getCity)
//				.map(City::getName);

		assertTrue(optUser.isPresent());

		Optional<Address> optAddress = optUser.map(User::getAddress);
		assertFalse(optAddress.isPresent());

		Optional<City> optCity = optAddress.map(Address::getCity);
		assertFalse(optCity.isPresent());
	}

	@Test
	public void testMapToTheEndWithNullsOrElse() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithNullAddress);
		String cityName = optUser
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName)
				.orElse("Default city name!");

		assertEquals("Default city name!", cityName);
	}

	@Test
	public void testMapToTheEndOrElse() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithAllFields);
		String cityName = optUser
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName)
				.orElse("Default city name!");

		assertEquals("Belgrade", cityName);
	}

	@Test
	public void testMapToTheEndWithNullsOrElseGet() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithNullAddress);
		Address address = optUser
				.map(User::getAddress)
				.orElseGet(() -> addressRepository.getByUserName(userWithNullAddress.getName()));

		assertNotNull(address);
	}

	@Test
	public void testMultipleMapsMultipleObjects() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithAllFields);

		Set<Optional<Address>> set = new HashSet<>();

		for (int i = 0; i < 15; i++) {
			set.add(optUser.map(User::getAddress));
		}

		assertEquals(1, set.size());
	}

	@Test
	public void testMultipleFiltersMultipleObjects() throws Exception {
		Optional<User> optUser = Optional.ofNullable(userWithAllFields);

		Set<Optional<User>> set = new HashSet<>();

		for (int i = 0; i < 15; i++) {
			set.add(
					optUser.filter(userIn -> userIn.getName() != null)
							.filter(userIn -> userIn.getName().equals("One")));
		}
		assertEquals(1, set.size());
	}

	@Test
	public void testOrElseThrowWithReasonToThrow() throws Exception {
		try {
			Optional.ofNullable(userWithNullAddress)
					.map(User::getAddress)
					.orElseThrow(UserHasNoAddressException::new);
			fail();
		} catch (UserHasNoAddressException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testOrElseThrowWithoutReasonToThrow() throws Exception {
		try {
			Optional.ofNullable(userWithAllFields)
					.map(User::getAddress)
					.orElseThrow(UserHasNoAddressException::new);

			assertTrue(true);
		} catch (UserHasNoAddressException e) {
			fail();
		}
	}

	@Test
	public void testIfPresentOnNotEmpty() throws Exception {
		StringBuilder textBuilder = new StringBuilder();
		Optional.ofNullable(userWithAllFields)
				.ifPresent(user -> textBuilder.append(user.getName()));

		assertEquals(userWithAllFields.getName(), textBuilder.toString());
	}

	@Test
	public void testIfPresentOnEmpty() throws Exception {
		User nullUser = null;
		Optional.ofNullable(nullUser)
				.ifPresent(user -> fail());

		Optional.ofNullable(nullUser)
				.ifPresent(user -> user.getAddress());
	}

	@Test
	public void testGetOnPresent() throws Exception {
		Optional<Address> optAddress = Optional.ofNullable(userWithAllFields)
				.map(User::getAddress);
		Address address = optAddress.get();
		assertNotNull(address);
	}

	@Test
	public void testGetOnNotPresent() throws Exception {
		Optional<Address> optAddress = Optional.ofNullable(userWithNullAddress)
				.map(User::getAddress);
		try {
			Address address = optAddress.get();
			fail();
		} catch (NoSuchElementException e) {
		}
	}

	@Test
	public void testGetOnNotPresentDeep() throws Exception {
		Optional<String> opt = Optional.ofNullable(userWithNullAddress)
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName);
		try {
			String cityName = opt.get();
			fail();
		} catch (NoSuchElementException e) {
		}
	}


	@Test
	public void emptyOrElseAndOrElseGetSpeed() throws Exception {
		Instant before;
		Instant after;

		before = Instant.now();
		Optional.ofNullable(userWithNullAddress)
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName)
				.orElse(configurationService.getString("street.default.name"));
		after = Instant.now();
		Duration orElse = Duration.between(before, after);

		before = Instant.now();
		Optional.ofNullable(userWithNullAddress)
				.map(User::getAddress)
				.map(Address::getStreet)
				.orElseGet(() -> configurationService.getString("street.default.name"));
		after = Instant.now();
		Duration orElseGet = Duration.between(before, after);

		BigDecimal orElseMillis = BigDecimal.valueOf(orElse.toMillis());
		BigDecimal orElseGetMillis = BigDecimal.valueOf(orElseGet.toMillis());

		assertThat(orElseMillis, closeTo(orElseGetMillis, BigDecimal.valueOf(100)));
		// almost the same.
		System.out.println("Difference: " + orElse.minus(orElseGet).abs().toMillis());
	}

	@Test
	public void notEmptyOrElseAndOrElseGetSpeed() throws Exception {
		Instant before;
		Instant after;

		before = Instant.now();
		Optional.ofNullable(userWithAllFields)
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName)
				.orElseGet(() -> configurationService.getString("street.default.name"));
		after = Instant.now();
		Duration orElseGet = Duration.between(before, after);

		before = Instant.now();
		Optional.ofNullable(userWithAllFields)
				.map(User::getAddress)
				.map(Address::getCity)
				.map(City::getName)
				.orElse(configurationService.getString("street.default.name"));
		after = Instant.now();
		Duration orElse = Duration.between(before, after);

		BigDecimal orElseMillis = BigDecimal.valueOf(orElse.toMillis());
		BigDecimal orElseGetMillis = BigDecimal.valueOf(orElseGet.toMillis());

		assertThat(orElseMillis, not(closeTo(orElseGetMillis, BigDecimal.valueOf(500))));
		// There was no need to call a slow method.
		System.out.println("Difference: " + orElse.minus(orElseGet).abs().toMillis());
		System.out.println("orElse: " + orElseMillis);
		System.out.println("orElseGet: " + orElseGetMillis);
	}



	@Before
	public void before() {
		userWithAllFields = createUserWithAllFields();
		userWithNullAddress = createUserWithNullAddress();
	}

	private User createUserWithNullAddress() {
		User user = new User();
		user.setName("UserWithNullAddress");
		return user;
	}

	private User createUserWithAllFields() {
		User user = new User();
		user.setName("userWithAllFields");
		user.setAddress(createFullAddress());
		return user;
	}

	private Address createFullAddress() {
		Address address = new Address();
		address.setStreet("Kneza Mihaila");
		address.setCity(createFullCity());
		return address;
	}

	private City createFullCity() {
		City city = new City();
		city.setName("Belgrade");
		city.setCountry("Serbia");
		city.setPopulation(3000000);
		return city;
	}
}
