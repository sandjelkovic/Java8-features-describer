package com.saanx.service;

import org.springframework.util.StringUtils;

/**
 * @author ${sandjelkovic}
 * @date 13.1.17.
 */
public class ConfigurationService {
	public String getString(String key) {
		sleep();
		return StringUtils.replace(key, ".", "-");
	}

	private void sleep() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ignored) {}
	}
}
