package com.saanx;

import java.util.Optional;

public class AddressRepository {
	public Optional<Address> findByUserName(String name) {
		return Optional.empty();
	}

	public Address getByUserName(String name) {
		if ("UserWithNullAddress".equalsIgnoreCase(name)) {
			return new Address(null, "StreetName");
		}
		return null;
	}
}
